# quick-api-client

## Description

Simple java app that gets a list of NASA-funded projects using the [TechPort API](https://techport.nasa.gov/help/articles/api)

## Building and Running

```
gradlew build
java -jar app\build\libs\app.jar
```
